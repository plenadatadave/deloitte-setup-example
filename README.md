# Instructions

Client ABC, please follow the following steps to setup and deploy the bots in production. 

>>>
 Do not run the following commands, if you have contracted Plena Data for bot hosting.
>>>

**Note:** Setup process is the same as developer setup process, if you have done this in the past. 

- [x]  Please download the *.tar file. Unzip it. Then double click on .exe OR .dmb OR .sh file

`Get SETUP in 5 minutes!`

```plantuml
!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.1.0
skinparam defaultTextAlignment center
!include ICONURL/common.puml
!include ICONURL/font-awesome-5/gitlab.puml
!include ICONURL/font-awesome-5/java.puml
!include ICONURL/font-awesome-5/rocket.puml
!include ICONURL/font-awesome/newspaper_o.puml
FA_NEWSPAPER_O(news,LINK,node) #White {
FA5_GITLAB(gitlab,Download,node) #White
FA5_JAVA(java,Install,node) #White
FA5_ROCKET(rocket,Run Bots!,node) #White
}
gitlab ..> java
java ..> rocket
```

## GCP Setup

**Following process can be done directly from the console at: https://cloud.google.com**

_1_ Create a VPC network 
```
gcloud compute networks create custom-network1 \
    --subnet-mode custom
```

_2_ Specify a region, external IP addresses and SSL certification 
```
gcloud compute networks create custom-network1 \
    --subnet-mode custom

gcloud compute instances create nat-test-1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --network custom-network1 \
    --subnet subnet-us-east-192 \
    --zone us-east4-c \
    --no-address

gcloud compute firewall-rules create allow-ssh \
    --network custom-network1 \
    --source-ranges 35.235.240.0/20 \
    --allow tcp:22

```

## Bot workflow

>>>
**Note:** The following flowchart depicts the bot's workflow. 
>>>

```mermaid
graph TB

  SubGraph1 --> SubFlow
  subgraph "Fraud Detection"
  SubFlow( IED > .8)
  SubFlow -- Yes --> Flag
  SubFlow -- No --> Done
  end

  subgraph "Bot Start"
  Node1[Open ERP] --> Node2[Review Claim]
  Node2 --> SubGraph1[Approval]
  SubGraph1 --> FinalThing[Done]
end
```






## Building Requirement

Dependencies are most likely already installed by the dev, however
Please ensure you have the required dependencies before installing:

* Windows
  * windows-build-tools npm package (`npm install --global --production windows-build-tools` from an elevated PowerShell or CMD.exe)
* Mac
  * Xcode Command Line Tools.
* Linux
  * Python (v2.7 recommended, v3.x.x is not supported).
  * make.
  * A C/C++ compiler like GCC.
  * libxtst-dev and libpng++-dev (`sudo apt-get install libxtst-dev libpng++-dev`).

Install node-pd using npm:

```
npm install -g node-pd
```

Then build:

```
node-pd rebuild
```

See the [node-pd readme](https://github.com/nodejs/node-pd#installation) for more details.


## License

Plena Data (Annual 4 bots MAX)
 
Maintained by [Plena Data](https://plenadata.com).